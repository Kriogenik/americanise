package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// На случай, если в эту свалку залет кто-то, цифры напротив некоторых строк я ставлю для личного удобства
// К программе они никакого отношения не имеют

func main() {

	inFileName, outFileName, err := filenamesFromCommandLine() // 1
	if err != nil {
		fmt.Println(err) // 2
		os.Exit(1)
	}

	inFile, outFile := os.Stdin, os.Stdout // 3
	// Аналогичны
	// inFile := os.Stdin
	// outFile := os.Stdout

	if inFileName != "" {

		if inFile, err = os.Open(inFileName); err != nil {
			log.Fatal(err)
		}
		defer inFile.Close()

	}

	if outFileName != "" {

		if outFile, err = os.Create(outFileName); err != nil {
			log.Fatal(err)
		}
		defer outFile.Close() // 4

	}

	if err = americanise(inFile, outFile); err != nil {
		log.Fatal(err) // 5
	}

}

// Так, идея понятня, вроде даже проста в реализации
func filenamesFromCommandLine() (inFileName, outFileName string, err error) {

	if len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		err = fmt.Errorf("usage: %s [<]infile.txt [>]outfile.txt", filepath.Base(os.Args[0]))
		return "", "", err
	}

	if len(os.Args) > 1 {
		inFileName = os.Args[1]
		if len(os.Args) > 2 {
			outFileName = os.Args[2]
		}
	}

	if inFileName != "" && inFileName == outFileName {
		log.Fatal("wont overwrite the infile")
	}
	return inFileName, outFileName, nil

}

var britishAmerican = "british-american.txt"

func americanise(inFile io.Reader, outFile io.Writer) error {

	var err error

	reader := bufio.NewReader(inFile)
	writer := bufio.NewWriter(outFile)
	defer func() {
		if err == nil {
			err = writer.Flush()
		}
	}()

	var replacer func(string) string // ЧТО. ТЫ. ТАКОЕ?!
	if replacer, err = makeReplacerFunction(britishAmerican); err != nil {
		return err
	}

	wordRx := regexp.MustCompile("[A-Za-z]+") //А ЭТО?!?!?!

	// Мать моя женщина...
	eof := false
	for !eof {
		var line string
		line, err = reader.ReadString('\n')
		if err == io.EOF {
			err = nil  // в действительности признак io.EOF не является ошибкой
			eof = true // это вызовет прекращение цикла в следующей итерации
		} else if err != nil {
			return err // в случае настоящей ошибки выйти немедленно
		}

		line = wordRx.ReplaceAllStringFunc(line, replacer)
		if _, err = writer.WriteString(line); err != nil {

			return err
		}
	}

	return nil
}

func makeReplacerFunction(file string) (func(string) string, error) {
	rawBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	text := string(rawBytes)
	usForBritish := make(map[string]string)
	lines := strings.Split(text, "\n")

	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) == 2 {
			usForBritish[fields[0]] = fields[1]
		}
	}

	return func(word string) string {
		if usWord, found := usForBritish[word]; found {
			return usWord
		}
		return word
	}, nil

}
